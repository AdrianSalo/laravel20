<?php

use App\Http\Controllers\PhotoController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PruebaController;
use App\Http\Controllers\StudyController;
use App\Http\Controllers\ModuleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('prueba',function(){
    echo "hola mundo";
});
Route::get('prueba2',[PruebaController::class,'hola']);
Route::get('prueba2/{name}',[PruebaController::class,'saludoCompleto']);

Route::resource('photos',PhotoController::class);
Route::get('saludo', [PruebaController::class,'saludo']);

Route::get('tabla/{size}',[PruebaController::class,'tabla']);
Route::resource('studies',StudyController::class);
Route::resource('modules',ModuleController::class);
Route::resource('users',UserController::class);
Route::resource('roles',RoleController::class);


//si usas las 7 rutas cuidado con el orden
//Route::get('photos/create', [PhotoController::class,'create']);
//Route::post('photos', [PhotoController::class,'store']);
//Route::get('photos', [PhotoController::class,'index']);
//Route::get('photos/{id}', [PhotoController::class,'show']);
//Route::get('photos/{id}/edit', [PhotoController::class,'edit']);
//Route::put('photos/{id}', [PhotoController::class,'update']);
//Route::delete('photos/{id}', [PhotoController::class,'destroy]']);
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

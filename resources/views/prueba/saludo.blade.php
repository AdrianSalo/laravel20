<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    @if ($informal)
        Hola mundo
    @else
        Buenos dias mundo
    @endif
   <h1> Hola mundo!</h1>
   {{ $informal }}
   <h2>Lista de numeros</h2>
   @foreach ($numeros as $numero)
   <li>{{$numero}}</li>
   @endforeach
</body>
</html>
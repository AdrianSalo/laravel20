<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Modelo nº {{$module->id}}</h1>
    <ul>
        <li>
            <strong>ID del estudio</strong>
            {{$module->study_id}}
        </li>
        <li>
            <strong>Curso</strong>
            {{$module->course}}
        </li>
        <li>
            <strong>Nombre</strong>
            {{$module->name}}
        </li>
        <li>
            <strong>Código</strong>
            {{$module->code}}
        </li>
        <li>
            <strong>Nombre corto</strong>
            {{$module->short_name}}
        </li>
        <li>
            <strong>Abreviación</strong>
            {{$module->abreviation}}
        </li>
    </ul>
</body>
</html>
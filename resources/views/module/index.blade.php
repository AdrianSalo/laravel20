<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>Lista de modelos</h1>
    <table border="1">
        <tr>
            <th>ID del estudio</th>
            <th>Curso</th>
            <th>Nombre</th>
            <th>Código</th>
            <th>Nombre Corto</th>
            <th>Abreviación</th>
        </tr>
        @forelse ($modules as $module)
        <tr>
            <td>{{$module->study_id}}</td>
            <td>{{$module->course}}</td>
            <td>{{$module->name}}</td>
            <td>{{$module->code}}</td>
            <td>{{$module->short_name}}</td>
            <td>{{$module->abreviation}}</td>
            <td><a href="/modules/{{$module->id}}">Ver</a></td>
        </tr>
        @empty
        <tr>
            <td colspan="3">No hay modulos registrados</td>
        </tr>
        @endforelse
    </table>
</body>
</html>
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h1>Lista de Usuarios</h1>
            <table class="table table-striped">
                <tr>
                    <th>Nombre</th>
                    <th>Email</th>
                    <th>Tipo</th>
                </tr>
            <form action="/users" method="get">
                <input type="text" placeholder="filtro nombre" name="name" value="{{$name}}">
                <input type="text" placeholder="filtro role" name="role" value="{{$role}}">
                <input type="submit" value="filtrar">
            </form>
            @foreach ($users as $user)
                <tr>
                   <td>{{$user->name}}</td> 
                   <td>{{$user->email}}</td>
                   <td>{{$user->role->name}}</td>
                </tr>
            @endforeach
            </table>
        </div>
        {{$users->links("pagination::bootstrap-4")}}
    </div>
</div>
@endsection 

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Module;
class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Module::create([
            'study_id' => 1,
            'course' => 2,
            'name' => 'Desarrollo web en entorno servidor',
            'code' => '0613',
            'short_name' => 'servidor',
            'abreviation' => 'ser'
        ]);
        Module::create([
            'study_id' => 1,
            'course' => 2,
            'name' => 'Desarrollo web en entorno cliente',
            'code' => '0612',
            'short_name' => 'cliente',
            'abreviation' => 'cli'
        ]);
        Module::create([
            'study_id' => 1,
            'course' => 2,
            'name' => 'Despliegue de aplicaciones web',
            'code' => '0614',
            'short_name' => 'despliegue',
            'abreviation' => 'des'
        ]);
        Module::create([
            'study_id' => 1,
            'course' => 2,
            'name' => 'Desarrollo de interfaces web',
            'code' => '0615',
            'short_name' => 'interfaces',
            'abreviation' => 'int'
        ]);
        Module::create([
            'study_id' => 1,
            'course' => 2,
            'name' => 'Empresa e iniciativa emprendedora',
            'code' => '0618',
            'short_name' => 'empresa',
            'abreviation' => 'emp'
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PruebaController extends Controller
{
    public function hola()
    {
        return "Hola desde PruebaController";
    }
    public function saludoCompleto($name)
    {
        return "Hola $name desde PruebaController";
    }
    public function saludo(Request $request)
    {
        $informal= $request -> input('informal');
        $all = $request->all();
        $numeros = [1,2,3,4];
        if ($informal) {
            echo "hola";
        } else {
            echo "buenos dias";
        }

        return view('prueba.saludo',[
        'informal' => $informal,
        'numeros' => $numeros]);
        //va a buscar un fichero en el directorio views:
        //existe prueba/saludo.blade.php ??
        //existe prueba/saludo.php ??
        
    }
    public function tabla($size)
    {
        return view('prueba.tabla',['size' => $size]);
    }
}
